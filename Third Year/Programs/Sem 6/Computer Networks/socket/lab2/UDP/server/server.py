import socket

sock = socket.socket(socket.AF_INET,socket.SOCK_DGRAM)

serverAddress = ('localhost',8000)

sock.bind(serverAddress)

print(f'Server has established at {serverAddress[0]}')

while True:
    try:
        fileRequest = sock.recvfrom(1024)
        file = open(str(fileRequest[0].decode()),"r")
        print("File name received")
        print("Filename: ",fileRequest[0])
        line = file.read(1024)
        while(line):
            sock.sendto(line.encode(),fileRequest[1])
            line = file.read(1024)
        sock.close()
        break
    finally:
        sock.close()
    
