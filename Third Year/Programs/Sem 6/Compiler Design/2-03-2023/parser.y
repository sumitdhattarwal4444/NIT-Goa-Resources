%{
    #include<stdio.h>
%}

%token INTEGER ID  

%left '+' '-'
%left '*' '/'

%%
line: expr {
    printf("Result = %d\n", $$);
    return 0;
}

expr: expr '+' expr { $$ = $1 + $3;}
    | expr '-' expr { $$ = $1 - $3;}
    | expr '*' expr { $$ = $1 * $3;}
    | expr '/' expr { $$ = $1 / $3;}
    | '-' INTEGER { $$ = -$2;}
    | '-' ID { $$ = $2;}
    | INTEGER { $$ = $1;}
    | ID { $$ = $1;};

%%

int main(){
    printf("Enter the expression\n");
    yyparse();
}
int yyerror(char* s){
    printf("\nExpression is invalid\n");
}