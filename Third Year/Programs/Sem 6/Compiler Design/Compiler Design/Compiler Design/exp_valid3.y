%{
#include <stdio.h>
int yylex(void);
void yyerror(char *);
%}
%union
{
int integer;
float floating;
}
%token <floating> FLOAT <integer> INTEGER
%type <floating> expr
%left '+' '-'
%left '*' '/'
%%
program:
program expr '\n' { printf("Expression is valid,Value=%f\n", $2); }
|
;
expr:
FLOAT{ $$ = $1; }
| expr '+' expr { $$ = $1 + $3; }
| expr '-' expr { $$ = $1 - $3; }
| expr '*' expr { $$ = $1 * $3; } 
| expr '/' expr { $$ = $1 / $3; }
|'-' expr { $$ = -$2; } 
| '('expr')'{ $$ = $2; }   
;
%%
void yyerror(char *s) {
fprintf(stderr, "%s\n", s);
}
int main(void) {
yyparse();
return 0;
}