%{
    #include<stdio.h>
    int yylex(void);
    void yyerror(char *);
    int sym[26];
%}


%token ID
%token NUM
%token FLOAT

%%

expr: expr '+' term {$$=$1+$3; printf("+");}
    | expr '-' term {$$=$1-$3; printf("-");}
    | term {$$ = $1;}
    ;

term: term '*' value {$$=$1*$3; printf("*");}
    | term '/' value {$$=$1/$3; printf("/");}
    | value {$$ = $1;}
    ;

value: NUM {printf("%d",$1);}

%%

void yyerror(char *s){
    printf("%s Invalid Expression\n", s);
}

int main(void){
    printf("Input: ");
    yyparse();
    return 0;
}