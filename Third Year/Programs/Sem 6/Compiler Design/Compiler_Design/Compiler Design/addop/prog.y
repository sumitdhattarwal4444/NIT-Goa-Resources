%{
    #include<stdio.h>
    int yylex(void);
    void yyerror(char *);
    int sym[26];
%}


%token ID
%token NUM
%token FLOAT
%token AddOp

%%

expr: T R 
    ;

R: AddOp T { printf("+");} R
    |
    ;

T: NUM {printf("%d",$1);}

%%

void yyerror(char *s){
    printf("%s Invalid Expression\n", s);
}

int main(void){
    printf("Input: ");
    yyparse();
    return 0;
}