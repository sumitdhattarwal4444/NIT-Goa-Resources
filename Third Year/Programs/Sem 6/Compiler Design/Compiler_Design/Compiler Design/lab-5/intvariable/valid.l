%{
    #include<stdio.h>
    void yyerror(char *s);
    #include "y.tab.h"
%}

alpha [A-Za-z]
digit [0-9]
%%

{digit}+ {
    yylval = atoi(yytext);
    return NUM;
}
{alpha} {
    yylval = *yytext - 'a';
    return ID;
}
[-+] {return *yytext;}
.;
%%
int yywrap(void){
    return 1;
}
