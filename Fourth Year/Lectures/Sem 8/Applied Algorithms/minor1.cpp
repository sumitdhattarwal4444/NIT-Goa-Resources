#include<bits/stdc++.h>
#define rep(i,a,b) for(int i=a;i<b;i++)
#define rrep(i,a,b) for(int i=a;i>=b;i--)
#define int long long
#define ull unsigned ll
#define take(n) int n;cin>>n
#define mod 1000000007
#define pii pair<int,int>
#define v(i) vector<i>
#define mp(a,b) make_pair(a,b)
#define pb(a) push_back(a)
#define pp pop_back()
#define array(n,name) vector<int> name(n);
#define takearray(n,name) rep(i,0,n) cin>>name[i];
#define printarray(n,nums) rep(i,0,n) cout<<nums[i]<<" ";
#define Zubin ios::sync_with_stdio(false);
#define Shah cin.tie(NULL);cout.tie(0);
#define nl cout<<endl;
using namespace std;

int xcor[4]={-1,0,0,+1};
int ycor[4]={0,-1,1,0};

int recursiveEngine(vector<int> &nums,int n,vector<pair<int,int>> &intervals,int index){
    int left = index;
    int sum = 0;
    for(int i=left;i<n;i++){
        sum+=nums[i];
        if(sum<0){
            intervals.push_back({index,i-1});
            if(!recursiveEngine(nums,n,intervals,i+1));
        }
    }
    return 0;
}
// 3,-5,4,-8,30,-7,5,-9,7
// 2,1,7
void positiveIntervals(vector<int> &nums,int n){
    vector<vector<int>> intervals;
    vector<int> negatives;
    int sum = 0;
    int left = 0;
    rep(i,0,n){
        sum+=nums[i];
        if(sum<0){
            if(i>left){
                vector<int> temp = {left,i-1,sum-nums[i]};
                intervals.pb(temp);
                negatives.pb(nums[i]);
            }
            sum = 0;
            left = i+1;
        }
    }
    if(sum>0){
        vector<int> temp = {left,n-1,sum};
        intervals.pb(temp);
    }
    stack<int> st;
    int negIndex = 0;
    stack<int> negst;
    int intervalCount = intervals.size();
    printarray(negatives.size(), negatives);
    nl;
    rep(i,0,intervals.size()){
        if(negIndex==negatives.size()) break;
        if(st.empty() || st.top()+intervals[i][2]<negst.top()){
            cout<<"pu:"<<intervals[i][2]<<endl;
            st.push(intervals[i][2]);
            negst.push(negatives[negIndex]);
            negIndex++;
        }
        else{
            while(negst.size() && st.top()+intervals[i][2]+negst.top()>0){
                int current = st.top();
                cout<<i<<" po:"<<current<<endl;
                st.pop();
                st.push(current+intervals[i][2]+negst.top());
                cout<<"pu:"<<st.top()<<endl;
                negst.pop();
                intervalCount--;
            }
        }
    }
    cout<<intervalCount<<endl;
    rep(i,0,intervals.size()){
        rep(j,0,intervals[i].size()){
            cout<<intervals[i][j]<<" ";
        }
        nl;
    }
}
// 3, -2, 5, 1, 2, -6, -3, -10, -5, -14, -9, -11, -15
// 3,  0, 7, 3, 4, 0, 
#undef int 
int main(){
#define int long long
    Zubin Shah

    take(n);
    array(n,nums);
    takearray(n,nums);
    positiveIntervals(nums,n);

return 0;
}