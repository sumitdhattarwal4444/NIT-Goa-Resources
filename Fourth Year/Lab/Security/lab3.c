#include<stdio.h>
#include<stdlib.h>
#include "euclid.h"
#define take(n) scanf("%lld",&n)
int main(){

    // LL int k = 3;
    // // int divisors[3] = {3,5,7};
    // // int remainders[3] = {2,3,2};
    
    LL int k;
    printf("Enter the value of k: ");
    take(k);
    int *divisors = (int*) malloc(sizeof(int)*k);
    int *remainders = (int*) malloc(sizeof(int)*k);
    printf("Enter %d space separated relatively coprime divisors: ",k);
    for(int i=0;i<k;i++){
        take(divisors[i]);
    }
    printf("Enter %d space separated remainders: ",k);
    for(int i=0;i<k;i++){
        take(remainders[i]);
    }

    LL int ans = chineseRemainderTheorem(divisors,remainders,k);
    printf("The integer is %lld\n",ans);
return 0;
}