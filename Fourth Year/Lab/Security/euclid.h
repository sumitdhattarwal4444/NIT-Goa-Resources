#include<stdlib.h>
#include<time.h>
#include "utils.h"

long long int generateLLD(){
    srand(time(NULL));
    int shift = (long long int)rand()%49 ;
    long long int random = (long long int)rand() << shift | rand();
    return random;
}

long long int gcd( long long int a, long long int b){
    if(b==0) return a;
    if(a<0) a = -a;
    if(b<0) b = -b;
    return gcd(1LL*b,1LL*a%b);
}

typedef struct triplet{
    long long int first; // gcd
    long long int second; // value of x
    long long int third; // value of y
}triplet;

typedef struct pair{
    long long int first; // gcd
    long long int second; // value of x
}pair;

triplet extendedGCD(LL int a,LL int b){
    if(a<b) return extendedGCD(b,a);
    triplet ans;
    if(b==0){
        ans.first = a;
        ans.second = 1;
        ans.third = 0;
        return ans;
    }
    triplet next = extendedGCD(b,a%b);
    ans.first = next.first;
    ans.second = next.third;
    ans.third = next.second - (a/b)*next.third;
    return ans;
}

int power(int a,int k){
    // a is number
    // k is the power
    // n is the mod
    // output = a^k mod n
    //RepeatedSquareAndMultiply Algorithm
    int output=1;
    if(k==0) return output;
    if(k&1) output = a;
    int ans = a;
    int t = numBits(k);
    for(int i=1;i<=t;i++){
        ans = (ans*ans);
        if(k&(1<<i)){
            output = (output*ans);
        }
    }
    return output;

}

int powerMod(int a,int k,int n){
    // a is number
    // k is the power
    // n is the mod
    // output = a^k mod n
    //RepeatedSquareAndMultiply Algorithm
    int output=1;
    if(k==0) return output;
    if(k&1) output = a;
    int ans = a;
    int t = numBits(k);
    for(int i=1;i<=t;i++){
        ans = (ans*ans)%n;
        if(k&(1<<i)){
            output = (output*ans)%n;
        }
    }
    return output;
}

// pair diophantineEquation(LL int a,LL int b,LL int n){
//     LL int d = extendedGCD(a,b,)
// }

LL int* linearCongruenceEquation(LL int a,LL int b,LL int n){
    
    triplet t = extendedGCD(a,n);
    LL int d = t.first;
    LL int x0 = t.second;
    LL int y0 = t.third;
    if(b%d!=0){
        printf("Solutions do not exist");
        return NULL;
    }
    LL int *soln = (LL int*)malloc(sizeof(LL int)*d);
    for(int i=0;i<d;i++){
        soln[i] = t.second + (n/d)*i;
        soln[i] = (soln[i] + d)%n;
    }   
    return soln;

}

LL int multiplicativeModuloInverse(LL int a,LL int n){
    triplet t = extendedGCD(a,n);

    printf("%lld * (%lld) + %lld * (%lld) = %lld \n",a,t.second,n,t.third,t.first);
    LL d = t.first; // gcd
    if(d!=1){
        printf("Multiplicative Modulo Inverse does not exist\n");
        return -1;
    }
    LL x = (t.second)%n; //xnot + 0t
    return x;
}


LL int chineseRemainderTheorem(int *n,int *a,int k){
    LL int nProduct = 1;
    for(int i=0;i<k;i++){
        nProduct *= n[i];
    }
    LL int *N = (LL int *)malloc(sizeof(int)*k);
    for(int i=0;i<k;i++){
        N[i] = nProduct/n[i];
    }
    LL int *M = (LL int *)malloc(sizeof(int)*k);

    for(int i=0;i<k;i++){
        printf("%lld ",N[i]);
    }
    printf("\n");
    for(int i=0;i<k;i++){
        printf("%lld ",n[i]);
    }
    printf("\n");

    for(int i=0;i<k;i++){
        M[i] = multiplicativeModuloInverse(N[i],n[i]);
    }
    LL int xbar = 0;
    for(int i=0;i<k;i++){
        xbar += (a[i]*N[i]*M[i])%nProduct;
    }
    xbar = xbar%nProduct;
    xbar = (xbar + nProduct)%nProduct;
    return xbar;
}



int orderOfGroup(int n,int a,int *primes,int *powers,int k){
    int t = n;
    for(int i=1;i<=k;i++){
        t = t/power(primes[i-1],powers[i-1]);
        int a1 = powerMod(a,t,n);
        while(a1!=1){
            a1 = powerMod(a1,primes[i-1],n);
            t = t*primes[i-1];
        }
    }
    return t;
}

int findOrderGroup(int n,int a){
    int *primes = (int*) malloc(20*(sizeof(int)));
    int *powers = (int*) malloc(20*(sizeof(int)));

    int currentn = n;
    int p = 2;
    int index = 0;
    while(currentn>1){
        if(currentn%p==0){
            int count =0 ;
            while(currentn%p==0){
                currentn/=p;
                count++;
            }
            primes[index] = p;
            powers[index] = count;
            index++;
        }
        p++;
    }
    // for(int i=0;i<index;i++){
    //     printf("%d %d\n",primes[i],powers[i]);
    // }
    int size = index;
    int t = orderOfGroup(n,a,primes,powers,size);
    return t;
}

int jacobi(int n,int a){
    if(a<=1) return a;
    int e = 0;
    int s;
    int a1 = a;
    while(a1%2==0){
        e++;
        a1/=2;
    }
    if(e%2==0){
        s = 1;
    }
    else{
        if(n%8==1 || n%8==7){
            s = 1;
        }
        else if(n%8==2 && n%8==5){
            s = -1;
        }
    }
    if(n%4==3 && a1%4==3){
        s = -s;
    }
    int n1 = n%a1;
    if(a1==1){
        return s;
    }
    else {
        return s*jacobi(n1,a1);
    }
}


int solovayStrassenProbabilitisticTest(int n,int t){
    /*
        Input:  n // number under consideration of primality testing
                t // security parameter
        Output: 1 // n is a prime
                0 // n is not a prime
    */

   for(int i=1;i<=t;i++){
        srand(time(0));
        int a = rand()%(n-4) + 2;
        int r = powerMod(a,(n-1)/2,n);
        if(r!=1 && r!=n-1) return 0;

   }
}