#include<stdio.h>
#include<stdlib.h>
#include<time.h>
#include "../Euclidean Algo/modular_exponentiation.h"

void getRandS(int a ,int* r, int* s){
    int temp = a-1;
    *r = 0;
    *s = 0;
    while(temp%2 == 0){
        temp = temp/2;
        (*s)++;
    }
    *r = temp;
}   

int primalityTest(int n){
    long long a = rand() % (n-4) + 2;
    int r, s;
    getRandS(n, &r, &s);
    // printf("r = %d, s = %d\n", r, s);
    long long y = modularExpo(a, r, n);
    printf("%lld ^ %d mod %d = %lld\n", a, r, n, y);
    if(y != 1 && y != n-1) {
        int j = 1;
        while(j<=(s-1) && y != n-1){
            y = (y*y) % n;
            if(y == 1) return 0;
            j++;
        }
        if(y != n-1) return 0;
    }
    return 1;
}

int main(){
    printf("Enter an odd number: ");
    int n;
    scanf("%d", &n);
    printf("Enter Security Parameter: ");
    int t;
    scanf("%d", &t);
    srand(time(NULL));
    for(int i=0;i<t;i++){
        if(!primalityTest(n)){
            printf("Composite\n");
            return 0;
        }
    }
    printf("Prime\n");
    return 0;
}