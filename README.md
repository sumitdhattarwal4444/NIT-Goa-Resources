# Hello There! 👋 <br>
# Welcome to this repository!

## This repository is the ultimate one-stop-shop for all your academic resources needs.
## Find books📚, lecture slides🗈, programs💻, question papers📜, notes🗊, etc all treasured into this repository.

<div id="header" align="center">
  <img src="https://media.giphy.com/media/HYVZ1CGO7M3yMYVBxk/giphy.gif" width="30%"/>
</div>

## This repository aims to cache the flow of resources from seniors to juniors.
### Idea behind using Gitlab as a platform is to give Git exposure to tech students at the same time. Plus giving valuable credits to our proud contributors!
### Anybody can contribute their resources by following up instructions in [contribution.md](https://gitlab.com/coder-zs-cse/NIT-Goa-Resources/-/blob/main/CONTRIBUTING.md) file

### Do give a star ⭐ for the repo, if it was helpful to you in anyway

#### Sharing is Caring❤️

# Our Star Contributors 😎
| Name | Roll Number | LinkedIn |
|------|-------------|----------|
| Zubin Shah | 20CSE1030 | https://www.linkedin.com/in/zubinshah1/ |
| Ashish Singh | 21CSE1003 | https://www.linkedin.com/in/45h15h/ |
| Harsh Patel | 19CSE1019 | https://www.linkedin.com/in/harshpatel63/ |
| Niranjan Hebli | 20CSE1019 | https://www.linkedin.com/in/niranjan-hebli-333211211/ |
| Shreya Parsekar | 20CSE1034 | https://www.linkedin.com/in/shreya-maniksha-parsekar-7241a81b2/ |
| Shruti Patil | 21CSE1034 | https://www.linkedin.com/in/shruti-p-0724b0207 |
| Ayushi Kandpal | 22CSE1004 | https://www.linkedin.com/in/ayushi-k-b84b63219 |
| Souradeep Banerjee | 20CSE1035 | https://www.linkedin.com/in/souradeep-banerjee-302110226/ |
| Poonam Prabhugaonkar | 22CSE1014 | https://www.linkedin.com/in/poonam-prabhugaonkar-174365250/ |
| Aryan Shrivastav | 20CSE1005 | https://www.linkedin.com/in/aryanshrivastav/ | 
| Priya Gaude | 20CSE1025 | https://www.linkedin.com/in/priya-gaude-848990232/ | 
| Gabriel D'Souza | 22CSE1011 | https://www.linkedin.com/in/gabriel-d-souza-9a67b12b9/ | 
| Devansh Garg | 21CSE1008 | https://www.linkedin.com/in/garg-devansh/ |
